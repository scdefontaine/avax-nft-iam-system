import { Card, Typography } from "antd";
import React from "react";
import { useMoralis } from "react-moralis";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useVerifyMetadata } from "hooks/useVerifyMetadata";
import Transfer from "./Wallet/components/Transfer";
import NativeBalance from "./NativeBalance";
import Address from "./Address/Address";
import Blockie from "./Blockie";

const { Text } = Typography;

const styles = {
  title: {
    fontSize: "20px",
    fontWeight: "700",
  },
  text: {
    fontSize: "16px",
  },
  card: {
    boxShadow: "0 0.5rem 1.2rem rgb(189 197 209 / 20%)",
    border: "1px solid #e7eaf3",
    borderRadius: "0.5rem",
  },
  timeline: {
    marginBottom: "-45px",
  },
};

export default function QuickStart() {
  const { Moralis } = useMoralis();
  // const { saveFile } = useMoralisFile();
  const { generateMetadata } = useVerifyMetadata();
  // const { data: NFTBalances } = useNFTBalances();
  const [role, setRole] = React.useState("");

  const handleChange = (event) => {
    setRole(event.target.value);
  };

  const handleSubmit = () => {
    const metadata = generateMetadata(role);
    console.log(metadata);
    const file = new Moralis.File("role.json", { base64: btoa(metadata) });
    file.saveIPFS();
    console.log(file.ipfs());
  };

  return (
    <div style={{ display: "flex", gap: "10px" }}>
      <Card
        style={styles.card}
        title={
          <div style={styles.header}>
            <Blockie scale={5} avatar currentWallet style />
            <Address size="6" copyable />
            <NativeBalance />
          </div>
        }
      >
        <Transfer />
      </Card>
      <div>
        <Card
          style={styles.card}
          title={
            <>
              <Text strong>Select the role for the NFT</Text>
            </>
          }
        >
          <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Role</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={role}
                label="Role"
                onChange={handleChange}
              >
                <MenuItem value={"DEFAULT_ADMIN_ROLE"}>Admin</MenuItem>
                <MenuItem value={"MODERATOR_ROLE"}>Moderator</MenuItem>
                <MenuItem value={"MINTER_ROLE"}>Minter</MenuItem>
                <MenuItem value={"USER_ROLE"}>User</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Card>
        <Card style={{ marginTop: "10px", ...styles.card }}>
          <Button
            type="submit"
            variant="contained"
            component="span"
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Card>
      </div>
    </div>
  );
}
